import Wrapper from './Wrapper';

export const SearchBar = ({onChange,value,...props}) => {
  return (
    <Wrapper>
      <input type="text" onChange={onChange} value={ value}{...props} />
    </Wrapper>
  );
};

export default SearchBar;
